#+TITLE: Emacs Configuration
#+AUTHOR: Ali Alsaffar
#+EMAIL: ali@alsaffar.io
#+STARTUP: content
:HIDDEN:
#+OPTIONS: toc:t num:nil
:END:

* Basics

  Increase garbage collection threshold to a larger size to make it run less often and speeding up some operations.

  #+BEGIN_SRC emacs-lisp
    (setq gc-cons-threshold 20000000)
  #+END_SRC

  Follow symbolic links when opening a file.

  #+BEGIN_SRC emacs-lisp
    (setq vc-follow-symlinks t)
  #+END_SRC

  Append a new line to files when saving.

  #+BEGIN_SRC emacs-lisp
    (setq require-final-newline t)
  #+END_SRC

  Don't show the usual startup message.

  #+BEGIN_SRC emacs-lisp
    (setq inhibit-startup-message t)
  #+END_SRC

  Clear the default scratch buffer comment.

  #+BEGIN_SRC emacs-lisp
    (setq initial-scratch-message nil)
  #+END_SRC

  Automatically refresh buffers when their files change. Always have the files synced.

  #+BEGIN_SRC emacs-lisp
    (global-auto-revert-mode t)
  #+END_SRC

  Always show matching parenthesis right away.

  #+BEGIN_SRC emacs-lisp
    (show-paren-mode t)
    (setq show-paren-delay 0.0)
  #+END_SRC

  Setup fill column size for when automatically filling.
  
  #+BEGIN_SRC emacs-lisp
    (setq-default fill-column 80)
  #+END_SRC

  Yank at point rather than at the mouse point.

  #+BEGIN_SRC emacs-lisp
    (setq mouse-yank-at-point t)
  #+END_SRC

  Remove the tool, menu and scroll bars.

  #+BEGIN_SRC emacs-lisp
    (tool-bar-mode 0)
    (menu-bar-mode 0)
    (scroll-bar-mode -1)
  #+END_SRC

  Set the frame title to the current buffer's name.

  #+BEGIN_SRC emacs-lisp
    (setq frame-title-format '((:eval (buffer-name))))
  #+END_SRC

  Highlight the current line.

  #+BEGIN_SRC emacs-lisp
    (global-hl-line-mode)
  #+END_SRC

  Prettify symbols, such as lambdas.

  #+BEGIN_SRC emacs-lisp
    (global-prettify-symbols-mode t)
  #+END_SRC

  Display buffers in another widnow instead of the current one when pressing =C-x C-b=. Use =ibuffer=.

  #+BEGIN_SRC emacs-lisp
    (defalias 'list-buffers 'ibuffer-other-window)
  #+END_SRC

  Setup environment.

  #+BEGIN_SRC emacs-lisp
    (setenv "BROWSER" "firefox")
  #+END_SRC

  Store backups and autosaved files in =/tmp= as not not clutter the working directory.

  #+BEGIN_SRC emacs-lisp
    (setq backup-directory-alist
	    `((".*" . ,temporary-file-directory)))
    (setq auto-save-file-name-transforms
	    `((".*" ,temporary-file-directory t)))
  #+END_SRC
  
  Enable global visual line mode.

  #+BEGIN_SRC emacs-lisp
    (global-visual-line-mode t)
  #+END_SRC

  Disable the alarm bell.

  #+BEGIN_SRC emacs-lisp
  (setq ring-bell-function 'ignore)
  #+END_SRC

  Scroll logically (IMO). =Stolen from Howard Abrams=.

  #+BEGIN_SRC emacs-lisp
    (setq scroll-conservatively 10000
	  scroll-preserve-screen-position t)
  #+END_SRC

  Start the =emacsclient= server so I can do stuff in the terminal and have files open here by default.

  #+BEGIN_SRC emacs-lisp
    (server-start)
  #+END_SRC

* Packages
** =use-package=

  Refresh package contents and install =use-package= if it's not installed already.
  
  #+BEGIN_SRC emacs-lisp
    (unless (package-installed-p 'use-package)
      (package-refresh-contents)
      (package-install 'use-package))
  #+END_SRC

  Configure and require it.

  #+BEGIN_SRC emacs-lisp
    (setq use-package-verbose t)
    (require 'use-package)
  #+END_SRC

** =org-mode=

   Add =org-mode= source to the load path.

   #+BEGIN_SRC emacs-lisp
     (add-to-list 'load-path "~/.emacs.d/org-mode/lisp")
   #+END_SRC

   Now require it:

   #+BEGIN_SRC emacs-lisp
     (require 'org-install)
     (require 'org-drill)
   #+END_SRC

     Use =org-bullets= which turns asterisks into nicer looking ASCII symbols.

  #+BEGIN_SRC emacs-lisp
    (use-package org-bullets
      :ensure t
      :config
      (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
  #+END_SRC

  Change the default three periods after every heading into a nice arrow. (Thanks to hrs for the idea)

  #+BEGIN_SRC emacs-lisp
    (setq org-ellipsis "⤵")
  #+END_SRC

  Have source blocks do proper syntax highlighting and propper mode for each one when editing.

  #+BEGIN_SRC emacs-lisp
    (setq org-src-fontify-natively t)
    (setq org-src-tab-acts-natively t)
  #+END_SRC

   Add TODO keywords to default list:

   #+BEGIN_SRC emacs-lisp
     (setq org-todo-keywords '((sequence "TODO(t)" "WAITING(w)" "|" "DONE(d)" "CANCELLED(c)")))
   #+END_SRC


  Identify agenda files for displaying of TODOs.

  #+BEGIN_SRC emacs-lisp
    '(org-agenda-files
       (quote
	("~/personal/gtd/inbox.org" "~/personal/gtd/gtd.org" "~/personal/gtd/tickler.org")))
  #+END_SRC

   Setup refile targets.

   #+BEGIN_SRC emacs-lisp
     (setq org-refile-targets '(("~/personal/gtd/gtd.org" :maxlevel . 3)
				("~/personal/gtd/someday.org" :level . 1)
				("~/personal/gtd/tickler.org" :maxlevel . 2)))
   #+END_SRC

   Setup capture templates.

   #+BEGIN_SRC emacs-lisp
     (setq org-capture-templates '(("t" "Todo [inbox]" entry
				    (file+headline "~/personal/gtd/inbox.org" "Tasks")
				    "* TODO %i%?")
				   ("T" "Tickler" entry
				    (file+headline "~/personal/gtd/tickler.org" "Tickler")
				    "* %i%? \n %U")
				   ("b" "Blog Idea" entry
				    (file+headline "~/personal/blog/ideas.org" "Ideas"))))
   #+END_SRC

   Add languages to org-mode babel execution:

   #+BEGIN_SRC emacs-lisp
     (org-babel-do-load-languages 'org-babel-load-languages
	 '(
	     (shell . t)
	 )
     )
   #+END_SRC

   #+RESULTS:

** =which-key=

  Use =use-package= to install =which-key= and configure it.

  #+BEGIN_SRC emacs-lisp
    (use-package which-key
      :ensure t
      :config
      (which-key-mode))
  #+END_SRC

** =counsel=

  Use =use-package= to install =counsel=.

  #+BEGIN_SRC emacs-lisp
    (use-package counsel
      :ensure t)
  #+END_SRC

** =swiper=

  Use =use-package= to install and configure =swiper=. This replaces default search, find-file, and M-x.

  #+BEGIN_SRC emacs-lisp
    (use-package swiper
      :ensure t
      :config
      (progn
	(ivy-mode 1)
	(setq ivy-use-virtual-buffers t)
	(global-set-key "\C-s" 'swiper)
	(global-set-key (kbd "M-x") 'counsel-M-x)
	(global-set-key (kbd "C-x C-f") 'counsel-find-file)
	))
  #+END_SRC

** =dired=

  This uses =use-package= to install and confure =dired=.

  P.S. installed via git as melpa removed dired+ from its repositories

  #+BEGIN_SRC emacs-lisp
    (require 'dired+)
  #+END_SRC

  #+RESULTS:
  : dired+

** =ace-window=

  Install and configure =ace-window= with =use-package=.

  #+BEGIN_SRC emacs-lisp
    (use-package ace-window
      :ensure t
      :init
      (progn
	(global-set-key [remap other-window] 'ace-window)
	(custom-set-faces
	 '(aw-leading-char-face
	   ((t (:inherit ace-jump-face-forground :height 3.0)))))
	))   
  #+END_SRC
** =paredit=

     #+BEGIN_SRC emacs-lisp
    (use-package paredit
      :ensure t
      :config
      (progn
	(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
	(add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
	(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
	(add-hook 'ielm-mode-hook             #'enable-paredit-mode)
	(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
	(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
	(add-hook 'scheme-mode-hook           #'enable-paredit-mode)
	(add-hook 'clojure-mode-hook          #'enable-paredit-mode)
	(add-hook 'slime-repl-mode-hook (lambda () (paredit-mode +1)))))
  #+END_SRC
** =pdf-tools=

  #+BEGIN_SRC emacs-lisp
    (use-package pdf-tools
      :pin manual
      :config
      (pdf-tools-install)
      (setq-default pdf-view-display-size 'fit-page))
  #+END_SRC

  #+RESULTS:
  : t

** =htmlize=

  Use =use-package= to install =htmlize=.

  #+BEGIN_SRC emacs-lisp
    (use-package htmlize
      :ensure t)
  #+END_SRC
** =magit=

  #+BEGIN_SRC emacs-lisp
    (use-package magit
      :ensure t
      :bind ("C-x g" . magit-status))
  #+END_SRC

** =SLIME=
** =auto-complete=

  Install and configure =auto-complete= via =use-package=.
  #+BEGIN_SRC emacs-lisp
    (use-package auto-complete
      :ensure t
      :init
      (progn
	(ac-config-default)
	(global-auto-complete-mode t)
	))
  #+END_SRC

** =yasnippet=

  Install =yasnippet= and use =use-package= for that and its configuration.
  #+BEGIN_SRC emacs-lisp
    (use-package yasnippet
      :ensure t
      :init
      (yas-global-mode 1))
  #+END_SRC

** =ledger-mode=

   #+BEGIN_SRC emacs-lisp
     (use-package ledger-mode
       :ensure t)
   #+END_SRC

   #+RESULTS:

** =ox-gfm=
** =ox-hugo=
** =CIDER=
** =web-mode=
* Functions

  Kill the current buffer without prompting.

  #+BEGIN_SRC emacs-lisp
    (defun me/kill-current-buffer ()
      (interactive)
      (kill-buffer (current-buffer)))
  #+END_SRC
* Key Bindings
** =C-x=
*** =C-x k=

    Kill the currnt buffer without prompting.

    #+BEGIN_SRC emacs-lisp
      (global-set-key (kbd "C-x k") 'me/kill-current-buffer)
    #+END_SRC

** =C-c=
*** =C-c c=

    =org-capture=

    #+BEGIN_SRC emacs-lisp
      (global-set-key (kbd "C-c c") 'org-capture)
    #+END_SRC

*** =C-c a=
   
    =org-agenda=

    #+BEGIN_SRC emacs-lisp
      (global-set-key (kbd "C-c a") 'org-agenda)
    #+END_SRC

* Theme

  Setup zenburn theme and load it.

  #+BEGIN_SRC emacs-lisp
    (use-package zenburn-theme
      :ensure t
      :config
      (load-theme 'zenburn t))
  #+END_SRC
