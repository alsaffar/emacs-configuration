;;; Package --- Summary: Initialization file for emacs configuration.

;;; Commentary:
;;; This code will define package lists to initialize and then load
;;; my personal configuration org mode file via babel.

;;; Code:
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

(add-to-list 'load-path "~/.emacs.d/org-mode/lisp")
(add-to-list 'load-path "~/.emacs.d/org-mode/contrib/lisp")
(add-to-list 'load-path "~/.emacs.d/lisp")

(package-initialize)

(require 'org-install)
;(require )

(org-babel-load-file (expand-file-name "~/.emacs.d/configuration.org"))
;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (ledger-mode pdf-tools dired+ zenburn-theme which-key use-package paredit org-bullets magit htmlize counsel ace-window))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:inherit ace-jump-face-forground :height 3.0)))))
(put 'dired-find-alternate-file 'disabled nil)
